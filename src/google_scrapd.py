import requests;
import re;
import json;
import time;
import logging;
import pandas as pd
import cv2
import numpy as np
import urllib
from PIL import Image
from io import BytesIO
from urllib.parse import urlparse
from os.path import splitext, basename
from selenium import webdriver
import imagehash
import os

# Barcode data
df = pd.read_fwf('C:/Users/HP/Documents/barcod.txt')
ld = list(df['Barcode'])

# DUCKDUCKGO - SEARCH
logging.basicConfig(level=logging.DEBUG);
logger = logging.getLogger(__name__)

def search(keywords, max_results=None):
    url = 'https://duckduckgo.com/';
    params = {
    	'q': keywords
    };

    logger.debug("Hitting DuckDuckGo for Token");

    #   First make a request to above URL, and parse out the 'vqd'
    #   This is a special token, which should be used in the subsequent request
    res = requests.post(url, data=params)
    searchObj = re.search(r'vqd=([\d-]+)\&', res.text, re.M|re.I);

    if not searchObj:
        logger.error("Token Parsing Failed !");
        return -1;

    logger.debug("Obtained Token");

    headers = {
        'authority': 'duckduckgo.com',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'sec-fetch-dest': 'empty',
        'x-requested-with': 'XMLHttpRequest',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'referer': 'https://duckduckgo.com/',
        'accept-language': 'en-US,en;q=0.9',
    }

    params = (
        ('l', 'us-en'),
        ('o', 'json'),
        ('q', keywords),
        ('vqd', searchObj.group(1)),
        ('f', ',,,'),
        ('p', '1'),
        ('v7exp', 'a'),
    )

    requestUrl = url + "i.js";

    logger.debug("Hitting Url : %s", requestUrl);

    while True:
        while True:
            try:
                res = requests.get(requestUrl, headers=headers, params=params);
                data = json.loads(res.text);
                break;
            except ValueError as e:
                logger.debug("Hitting Url Failure - Sleep and Retry: %s", requestUrl);
                time.sleep(5);
                continue;

        logger.debug("Hitting Url Success : %s", requestUrl);
        dfd = printJson(data["results"],keywords);

        if 'next' not in data:
            logger.debug("No Next Page - Exiting");
            break
            exit(0);

        requestUrl = url + data["next"];
    return dfd

def printJson(objs,keywords):
    bd = []
    td = []
    tp = []
    md = []
    #wd = []
    #hd = []
    for obj in objs:
        bd.append(keywords)
        md.append(obj["image"])
        td.append(obj["title"])
        tp.append('DuckDuckGo')
        print("Width {0}, Height {1}".format(obj["width"], obj["height"]))
        print("Thumbnail {0}".format(obj["thumbnail"]));
        print("Url {0}".format(obj["url"]));
        print("Title {0}".format(obj["title"].encode('utf-8')));
        print("Image {0}".format(obj["image"]))
        print("__________")
    df = pd.DataFrame({'Barcode':bd,'Title':td,'Image':md,'Type':tp})
    return df  

# DUCKDUCKGO RESULT
dfd = pd.DataFrame({'Barcode':[],'Image':[],'Title':[],'Type':[]})
for i in ld:
    df = search(i)
    dfd = dfd.append(df)
dfd.index = range(dfd.shape[0])
nd = list(set(ld).symmetric_difference(set(dfd['Barcode'].unique())))

# GOOGLE SEARCH
def fetch_image_urls(query:str, max_links_to_fetch:int, wd:webdriver, sleep_between_interactions:int=1):
    def scroll_to_end(wd):
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(sleep_between_interactions)    
    
    # build the google query
    search_url = "https://www.google.com/search?safe=off&site=&tbm=isch&source=hp&q={q}&oq={q}&gs_l=img"

    # load the page
    wd.get(search_url.format(q=query))

    image_urls = []
    td = []
    bd = []
    tp = []
    image_count = 0
    results_start = 0
    while image_count < max_links_to_fetch:
        scroll_to_end(wd)

        # get all image thumbnail results
        thumbnail_results = wd.find_elements_by_css_selector("img.Q4LuWd")
        number_results = len(thumbnail_results)
        
        print(f"Found: {number_results} search results. Extracting links from {results_start}:{number_results}")
        
        for img in thumbnail_results[results_start:number_results]:
            # try to click every thumbnail such that we can get the real image behind it
            try:
                img.click()
                time.sleep(sleep_between_interactions)
            except Exception:
                continue

            # extract image urls    
            actual_images = wd.find_elements_by_css_selector('img.n3VNCb')
            for actual_image in actual_images:
                if actual_image.get_attribute('src') and 'http' in actual_image.get_attribute('src'):
                    image_urls.append(actual_image.get_attribute('src'))
                    td.append(os.path.basename(urlparse(actual_image.get_attribute('src')).path))
                    bd.append(query)
                    tp.append('Google')

            image_count = len(image_urls)
            df = pd.DataFrame({'Barcode':bd,'Image':image_urls,'Title':td,'Type':tp})
            
            if len(image_urls) >= max_links_to_fetch:
                print(f"Found: {len(image_urls)} image links, done!")
                break
        else:
            print("Found:", len(image_urls), "image links, looking for more ...")
            time.sleep(10)
            return
            load_more_button = wd.find_element_by_css_selector(".mye4qd")
            if load_more_button:
                wd.execute_script("document.querySelector('.mye4qd').click();")

        # move the result startpoint further down
        results_start = len(thumbnail_results)

    return df

# GOOGLE RESULT
with webdriver.Chrome(executable_path='C:/Users/HP/Downloads/chromedriver_win32/chromedriver.exe') as wd:
    for i in nd:
        df = fetch_image_urls(i, 5, wd=wd, sleep_between_interactions=0.5)
        if df is not None:
            dfd = dfd.append(df)
dfd.index = range(dfd.shape[0])
nd = list(set(ld).symmetric_difference(set(dfd['Barcode'].unique())))
print('The barcode does not have information')
print(nd)
dnd = pd.DataFrame({'Barcode':nd})

# Storing the information
dfd.to_csv('G:/R/Feature_Barcode_ftd.csv')
dnd.to_csv('G:/R/Feature_Barcode_not_ftd.csv')

# DOWNLOADING IMAGES
imd = []
def url_to_image(url):
    headers = {
    'authority': 'duckduckgo.com',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'sec-fetch-dest': 'empty',
    'x-requested-with': 'XMLHttpRequest',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'referer': 'https://duckduckgo.com/',
    'accept-language': 'en-US,en;q=0.9',
    }
    request = urllib.request.Request(url,None,headers)
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV forma
    resp = urllib.request.urlopen(request)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    # return the image
    return image

for i in range(dfd.shape[0]):
    picture_page = dfd['Image'][i]
    disassembled = urlparse(picture_page)
    filename, file_ext = splitext(basename(disassembled.path))
    if file_ext is None:
        file_ext = '.jpg'
    md = str(int(dfd['Barcode'][i]))+'_'+str(i)+file_ext
    imd.append(md)
    
    try:
        response = requests.get(dfd['Image'][i])

    except Exception as e:
        print(f"ERROR - Could not download {dfd['Image'][i]} - {e}")
 
    try:
        img = Image.open(BytesIO(response.content))
        img.save(md)
        
    except Exception as e:
        try:  
            image = url_to_image(df['Image'][i])
            cv2.imwrite(md,image)
        except:
            print(f"ERROR - Could not download {{dfd['Image'][i]}} - {e}")

dfd['Fild'] = imd

# Selection
df = dfd.drop_duplicates(subset = ['Barcode'])
df.index = range(df.shape[0])
df.to_csv("G:/R/Feature_Barcode_image_ftd.csv")

# IMAGE HASH
hd = []
for i in df['Image']:
    print(i)
    response = requests.get(i)
    print((imagehash.phash(Image.open(BytesIO(response.content)))))
    hd.append(str(imagehash.phash(Image.open(BytesIO(response.content)))))
    
dhd = pd.DataFrame({'Barcode':list(df['Barcode']),'Hash':hd})

# SCRAPPED DATA
rd = pd.read_csv("G:/R/Feature_hash_web_ftd.csv")

# COMPARE HASH

# MATCHING BARCODE
rdf = pd.merge(dhd,rd,how="inner")
print("Matching")
print(rdf)

# NON MATCH BARCODE
mdf = dhd[~dhd.isin(rd.to_dict('l')).any(1)]
print("Non Matching")
print(mdf)
rd = rd.append(mdf)
rd.index = range(rd.shape[0])
rd.to_csv("G:/R/Feature_hash_web_ftd.csv")

