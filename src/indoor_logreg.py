import numpy as np
import pandas as pd

# Data
df = pd.read_csv("indtrackd.csv")
dfd = df.dropna(axis=0, subset=['pos'])
dfd = dfd.dropna(axis=0, subset=[['1','2','3','4','5','6']],thresh=1)
dfd = dfd.fillna(-200,axis=0)
dfd.replace('near_min_1',1,inplace=True)
dfd.replace('near_min_2',2,inplace=True)
dfd.index = range(dfd.shape[0])


# Features and Target
X = dfd.iloc[:,[2,3,4,5,6,7]].values
y = dfd.iloc[:,8].values

# Split the Data Train and Test
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3,random_state=0)

# data normalization with sklearn
from sklearn.preprocessing import MinMaxScaler
norm = MinMaxScaler().fit(X_train)
X_train_norm = norm.transform(X_train)
X_test_norm = norm.transform(X_test)

# Logistic Regression
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
logreg = LogisticRegression()
logreg.fit(X_train_norm, y_train)

# Prediction
y_pred = logreg.predict(X_test_norm)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(X_test_norm, y_test)))

# CM & Metrics
from sklearn.metrics import confusion_matrix
confusion_matrix = confusion_matrix(y_test, y_pred)
print(confusion_matrix)

from sklearn.metrics import classification_report
print(classification_report(y_test, y_pred))
